﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using EmailProcessing.Services.Client;

namespace EmailProcessing
{
    public class Program
    {
        public static void Main(string[] args)
        { }
        public static bool SendEmail(string content, byte[] contentByte)
        {
            try
            {
                var _clientEmail = new EmailProcessing.Services.Client.EmailService.EmailProcessingClient();
                var rq = new Services.Client.EmailService.EmailProcessingRQ();
                rq.To = new Services.Client.EmailService.To { ToAddress = "testedevviajanet@gmail.com" , ToName="xxx", Encoding="UTF-8"};
                rq.From = new Services.Client.EmailService.From { FromAddress = "testedevviajanet@gmail.com", FromName = "teste",Encoding="UTF-8" };
            
                rq.Message = new Services.Client.EmailService.Message { Body = content, Subject = "TESTE", ByteBody=contentByte };
                rq.Message.BodyEncoding = "UTF-8";
                rq.GoogleAnalytic = new Services.Client.EmailService.GoogleAnalytics();
                rq.SendGridParameter = new Services.Client.EmailService.SendGridParameters() { Category = "ViajaNet-reserva_solicitada" };
                var res = _clientEmail.SendMail(rq);
                //MailMessage mail = new MailMessage();
                //SmtpClient client = new SmtpClient();
                //client.Port = 587;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.UseDefaultCredentials = false;
                //client.Host = "smtp.sendgrid.net";

                //NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["user"], "vn.2012.admin");
                //client.Credentials = SMTPUserInfo;

                //mail.To.Add(new MailAddress("testedevviajanet@gmail.com"));
                //mail.From = new MailAddress("testedevviajanet@gmail.com");

                //mail.Subject = "TESTE";
                //mail.Body = content;
                //mail.IsBodyHtml = true;
                //client.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }

        }
    }
}
