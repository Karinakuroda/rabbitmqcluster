# README #

# RabbitMQClusterDockerSample
RabbitMQ Cluster com Docker

* Instalar e habilitar docker!
* P/Habilitar:
 sudo systemctl enable docker
 sudo systemctl status docker
 sudo systemctl start docker



* P/ FAZER BUILD:
docker build -t karinakuroda/rabbitmq-cluster .
* P/ SUBIR NO DOCKER:
docker-compose up -d
* P/ VISUALIZAR OS LOGS:
docker-compose logs
* P/DERRUBAR:
docker-compose down